package com.example.EmployeeCrud;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController @CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeRepository repo;
	
	@GetMapping("/listall")
	public @ResponseBody Iterable<Employee> listAll(){
		return repo.findAll();
	}
	
	@GetMapping("/byid/{id}")
	public Optional<Employee> getById(@PathVariable int id){
		return repo.findById(id);
	}
	
	@PostMapping("/add")
	public void add(@RequestBody Employee emp) {
		repo.save(emp);
	}
	
	@PutMapping("/update/{id}")  								
	public void updateData(@PathVariable int id,@RequestBody Employee emp)   
	{  
	repo.save(emp);   
	}  
	
	@DeleteMapping("/delete/{id}")  					
	public void deleteData(@PathVariable int id)   
	{  
		repo.deleteById(id);  
	}

}
