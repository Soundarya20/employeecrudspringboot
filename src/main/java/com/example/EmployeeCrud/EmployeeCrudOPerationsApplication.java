package com.example.EmployeeCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeCrudOPerationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeCrudOPerationsApplication.class, args);
	}

}
