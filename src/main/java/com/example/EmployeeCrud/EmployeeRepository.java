package com.example.EmployeeCrud;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository <Employee,Integer>{

}
